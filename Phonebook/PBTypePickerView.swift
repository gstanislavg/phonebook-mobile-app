//
//  PBTypePickerView.swift
//  Phonebook
//
//  Created by Stanislav Georgiev on 17.12.18.
//  Copyright © 2018 MacUSER. All rights reserved.
//

import UIKit

// MARK: Enum

enum PickerType {
    case none
    case address
    case phoneNumber
    case email
}

class PBTypePickerView: UIPickerView {
    
    
    // MARK: Variables
    
    var type: PickerType = .none
    
    weak var customDelegate: PickerViewSelectedTypeDelegate?
    
    
    // MARK: Initialization
    
    init(frame: CGRect = .zero, type: PickerType) {
        super.init(frame: frame)
        self.delegate   = self
        self.dataSource = self
        self.type       = type
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PBTypePickerView: UIPickerViewDelegate {
    
    // MARK: Picker View Delegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch self.type {
        case .address:
            return CreateContactViewController.addressTypes[row]
        case .email:
            return CreateContactViewController.emailTypes[row]
        case .phoneNumber:
            return CreateContactViewController.phoneNumberTypes[row]
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch self.type {
        case .address:
            self.customDelegate?.didSelectType(type: CreateContactViewController.addressTypes[row])
        case .email:
            self.customDelegate?.didSelectType(type: CreateContactViewController.emailTypes[row])
        case .phoneNumber:
            self.customDelegate?.didSelectType(type: CreateContactViewController.phoneNumberTypes[row])
        default:
            return
        }
    }
    
}

extension PBTypePickerView: UIPickerViewDataSource {
    
    // MARK: Picker View Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch self.type {
        case .address:
            return CreateContactViewController.addressTypes.count
        case .email:
            return CreateContactViewController.emailTypes.count
        case .phoneNumber:
            return CreateContactViewController.phoneNumberTypes.count
        default:
            return 0
        }
    }

}

protocol PickerViewSelectedTypeDelegate: class {
    
    func didSelectType(type: String)
}
