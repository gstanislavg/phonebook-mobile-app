//
//  PBPickerViewToolbar.swift
//  Phonebook
//
//  Created by Stanislav Georgiev on 17.12.18.
//  Copyright © 2018 MacUSER. All rights reserved.
//

import UIKit

class PBPickerViewToolbar: UIToolbar {
    
    init(frame: CGRect = .zero, doneAction: Selector, cancelAction: Selector? = nil, presenter: UITableViewCell) {
        super.init(frame: frame)
        self.sizeToFit()
        self.addButton(doneAction: doneAction, cancelAction: cancelAction, presenter: presenter)
        self.isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addButton(doneAction: Selector, cancelAction: Selector?, presenter: UITableViewCell) {
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: presenter, action: doneAction)
        
        var buttons: [UIBarButtonItem] = [doneButton]
        
        if let cancelAction = cancelAction {
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: presenter, action: cancelAction)
            buttons.append(cancelButton)
        }
        
        self.setItems(buttons, animated: false)
    }
}

