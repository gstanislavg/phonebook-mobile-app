//
//  DBManager.swift
//  Phonebook
//
//  Created by MacUSER on 10/20/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

// https://www.appcoda.com/fmdb-sqlite-database/ - Link if errors occur

import UIKit

class DataBaseManager: NSObject {
    
    
    // MARK: Singleton
    
    static let sharedInstance: DataBaseManager = DataBaseManager()
    
    
    // MARK: Constants
    
    private let databaseFileName = "phonebook.sqlite"
    
    
    // MARK: Varibles
    
    private var pathToDatabase: String!
    
    private var database: FMDatabase!
    
    
    // MARK: Initializers
    
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        self.pathToDatabase = documentsDirectory.appending("/\(self.databaseFileName)")
    }
    
    
    // MARK: Private Methods
    
    private func openDatabase() -> Bool {
        if self.database == nil {
            if FileManager.default.fileExists(atPath: self.pathToDatabase) {
                self.database = FMDatabase(path: self.pathToDatabase)
            }
        }
        
        if self.database != nil {
            if self.database.open() {
                return true
            }
        }
        
        return false
    }
    
    
    // MARK: Public Methods
    
    func createDatabaseIfItsMissing() {
        let createContactsTableQuery = "create table \(PBStrings.TableNames.contactsTable) (\(PBStrings.ContactsTableFields.fieldContactId) integer PRIMARY KEY autoincrement not null, \(PBStrings.ContactsTableFields.fieldDateAdded) integer not null, \(PBStrings.ContactsTableFields.fieldFirstName) text, \(PBStrings.ContactsTableFields.fieldLastName) text,  \(PBStrings.ContactsTableFields.fieldCompany) text, \(PBStrings.ContactsTableFields.fieldProfileImage) blob, \(PBStrings.ContactsTableFields.fieldProfileImageThumbnail) text, \(PBStrings.ContactsTableFields.fieldUrl) text, \(PBStrings.ContactsTableFields.fieldNotes) text, \(PBStrings.ContactsTableFields.fieldBirthDate) text, \(PBStrings.ContactsTableFields.fieldJobTitle) text, \(PBStrings.ContactsTableFields.fieldPrefixId) integer, \(PBStrings.ContactsTableFields.fieldIsFavourite) integer, \(PBStrings.ContactsTableFields.fieldIsBlocked) integer);"
        
        let createPhoneNumbersTableQueryString = "create table \(PBStrings.TableNames.phoneNumbersTable) (\(PBStrings.PhoneNumberTableFields.fieldPhoneNumberId) integer primary key autoincrement not null, \(PBStrings.PhoneNumberTableFields.fieldPhoneNumberContactId) integer, \(PBStrings.PhoneNumberTableFields.fieldPhoneNumberType) integer, \(PBStrings.PhoneNumberTableFields.fieldPhoneNumber) text, FOREIGN KEY(\(PBStrings.PhoneNumberTableFields.fieldPhoneNumberType)) REFERENCES \(PBStrings.TableNames.phoneNumberTypesTable)(\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeId)), FOREIGN KEY(\(PBStrings.PhoneNumberTableFields.fieldPhoneNumberContactId)) REFERENCES \(PBStrings.TableNames.contactsTable)(\(PBStrings.ContactsTableFields.fieldContactId)));"
        
        let createPhoneNumberTypesTableQueryString = "create table \(PBStrings.TableNames.phoneNumberTypesTable) (\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeId) integer PRIMARY KEY autoincrement not null, \(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName) text);"
        
        let createEmailsTableQueryString = "create table \(PBStrings.TableNames.emailsTable) (\(PBStrings.EmailTableFields.fieldEmailId) integer PRIMARY KEY autoincrement not null, \(PBStrings.EmailTableFields.fieldEmailContactId) integer, \(PBStrings.EmailTableFields.fieldEmail) text, \(PBStrings.EmailTableFields.fieldEmailType) integer, FOREIGN KEY(\(PBStrings.EmailTableFields.fieldEmailContactId)) REFERENCES \(PBStrings.TableNames.contactsTable)(\(PBStrings.ContactsTableFields.fieldContactId)), FOREIGN KEY(\(PBStrings.EmailTableFields.fieldEmailType)) REFERENCES \(PBStrings.TableNames.emailTypesTable)(\(PBStrings.EmailTypesTableFields.fieldEmailTypeId)));"
        
        let createEmailTypesTableQueryString = "create table \(PBStrings.TableNames.emailTypesTable) (\(PBStrings.EmailTypesTableFields.fieldEmailTypeId) integer PRIMARY KEY autoincrement not null, \(PBStrings.EmailTypesTableFields.fieldEmailTypeName) text);"
        
        let createAddressTableQuery = "create table \(PBStrings.TableNames.addressesTable)(\(PBStrings.AddressesTableFields.fieldAddressId) integer PRIMARY KEY autoincrement not null, \(PBStrings.AddressesTableFields.fieldAddressContactId) integer, \(PBStrings.AddressesTableFields.fieldAddressCountry) text, \(PBStrings.AddressesTableFields.fieldAddressCity) text, \(PBStrings.AddressesTableFields.fieldAddressStreet) text, \(PBStrings.AddressesTableFields.fieldAddressZipCode) text, \(PBStrings.AddressesTableFields.fieldAddressType) integer, FOREIGN KEY(\(PBStrings.AddressesTableFields.fieldAddressContactId)) REFERENCES \(PBStrings.TableNames.contactsTable)(\(PBStrings.ContactsTableFields.fieldContactId)), FOREIGN KEY(\(PBStrings.AddressesTableFields.fieldAddressType)) REFERENCES \(PBStrings.TableNames.addressTypesTable)(\(PBStrings.AddressTypesTableFields.fieldAddressTypeId)));"
        
        let createAddressTypesTableQuery = "create table \(PBStrings.TableNames.addressTypesTable)(\(PBStrings.AddressTypesTableFields.fieldAddressTypeId) integer PRIMARY KEY autoincrement not null, \(PBStrings.AddressTypesTableFields.fieldAddressTypeName) text);"
        
        let insertPhoneTypes = "insert into \(PBStrings.TableNames.phoneNumberTypesTable)(\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName)) values ('\(PBStrings.DefaultTypes.mobile)'); insert into \(PBStrings.TableNames.phoneNumberTypesTable)(\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName)) values ('\(PBStrings.DefaultTypes.home)'); insert into \(PBStrings.TableNames.phoneNumberTypesTable)(\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName)) values ('\(PBStrings.DefaultTypes.work)'); insert into \(PBStrings.TableNames.phoneNumberTypesTable)(\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName)) values ('\(PBStrings.DefaultTypes.other)');" // values ('home')
        
        let insertEmailTypes = "insert into \(PBStrings.TableNames.emailTypesTable)(\(PBStrings.EmailTypesTableFields.fieldEmailTypeName)) values ('\(PBStrings.DefaultTypes.home)'); insert into \(PBStrings.TableNames.emailTypesTable)(\(PBStrings.EmailTypesTableFields.fieldEmailTypeName)) values ('\(PBStrings.DefaultTypes.work)');"
        
        let insertAddressTypes = "insert into \(PBStrings.TableNames.addressTypesTable)(\(PBStrings.AddressTypesTableFields.fieldAddressTypeName)) values ('\(PBStrings.DefaultTypes.home)'); insert into \(PBStrings.TableNames.addressTypesTable)(\(PBStrings.AddressTypesTableFields.fieldAddressTypeName)) values ('\(PBStrings.DefaultTypes.work)');"
        
        if !FileManager.default.fileExists(atPath: self.pathToDatabase) {
            self.database = FMDatabase(path: self.pathToDatabase ?? "")
            
            guard let database = self.database else {
                print("Could not open the database.")
                return
            }
            
            if database.open() {
                
                database.executeStatements("\(createContactsTableQuery) \(createPhoneNumberTypesTableQueryString) \(createPhoneNumbersTableQueryString) \(createEmailTypesTableQueryString) \(createEmailsTableQueryString) \(createAddressTypesTableQuery) \(createAddressTableQuery)")
    
                database.executeStatements("\(insertPhoneTypes) \(insertEmailTypes) \(insertAddressTypes)")
                
                database.close()
            }
        }
    }
    
    func insertContact(contact: Contact) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        var birthdayInMilliseconds: Int?
        
        if let birthday = contact.birthDate {
            let birthdayDate = dateFormatter.date(from: birthday)
            birthdayInMilliseconds = Int((birthdayDate?.timeIntervalSince1970)! * 1000)
        }
        
        let dateAddedInMiliseconds = Int(Date().timeIntervalSince1970 * 1000)
        
        let queue = FMDatabaseQueue(path: self.pathToDatabase)
        queue?.inTransaction { database, rollback in
            do {
                let insertContactQuery = "insert into \(PBStrings.TableNames.contactsTable) (\(PBStrings.ContactsTableFields.fieldDateAdded), \(PBStrings.ContactsTableFields.fieldFirstName), \(PBStrings.ContactsTableFields.fieldLastName),  \(PBStrings.ContactsTableFields.fieldCompany), \(PBStrings.ContactsTableFields.fieldProfileImage), \(PBStrings.ContactsTableFields.fieldProfileImageThumbnail), \(PBStrings.ContactsTableFields.fieldUrl), \(PBStrings.ContactsTableFields.fieldNotes), \(PBStrings.ContactsTableFields.fieldBirthDate), \(PBStrings.ContactsTableFields.fieldJobTitle), \(PBStrings.ContactsTableFields.fieldPrefixId), \(PBStrings.ContactsTableFields.fieldIsFavourite), \(PBStrings.ContactsTableFields.fieldIsBlocked)) values (?,?,?,?,?,?,?,?,?,?,?,?,?)"
                
                try database.executeUpdate(insertContactQuery, values: [dateAddedInMiliseconds, contact.firstName ?? NSNull(), contact.lastName ?? NSNull(), contact.company ?? NSNull(), contact.profileImage ?? NSNull(), contact.profilePictureThumbnail ?? NSNull(), contact.url ?? NSNull(), contact.note ?? NSNull(), birthdayInMilliseconds ?? NSNull(), contact.jobTitle ?? NSNull(), contact.prefix ?? NSNull(), contact.isFavorite, contact.isBlocked])
                
                let currentContactId = Int(database.lastInsertRowId)

                let insertPhoneNumberQuery = "insert into \(PBStrings.TableNames.phoneNumbersTable) (\(PBStrings.PhoneNumberTableFields.fieldPhoneNumberContactId), \(PBStrings.PhoneNumberTableFields.fieldPhoneNumberType), \(PBStrings.PhoneNumberTableFields.fieldPhoneNumber)) values (?,(select \(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeId) from \(PBStrings.TableNames.phoneNumberTypesTable) where \(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName) = ?),?)"
                for phone in contact.phoneNumbers {
                    try database.executeUpdate(insertPhoneNumberQuery, values: [currentContactId, phone.type, phone.number])
                }
                
                let insertEmailQuery = "insert into \(PBStrings.TableNames.emailsTable) (\(PBStrings.EmailTableFields.fieldEmailContactId), \(PBStrings.EmailTableFields.fieldEmail), \(PBStrings.EmailTableFields.fieldEmailType)) values (?,?,(select \(PBStrings.EmailTypesTableFields.fieldEmailTypeId) from \(PBStrings.TableNames.emailTypesTable) where \(PBStrings.EmailTypesTableFields.fieldEmailTypeName) = ?))"
                for email in contact.emails {
                    try database.executeUpdate(insertEmailQuery, values: [currentContactId, email.email, email.type])
                }
                
                let insertAddressQuery = "insert into \(PBStrings.TableNames.addressesTable) (\(PBStrings.AddressesTableFields.fieldAddressContactId), \(PBStrings.AddressesTableFields.fieldAddressCountry), \(PBStrings.AddressesTableFields.fieldAddressCity), \(PBStrings.AddressesTableFields.fieldAddressStreet), \(PBStrings.AddressesTableFields.fieldAddressZipCode), \(PBStrings.AddressesTableFields.fieldAddressType)) values (?,?,?,?,?,(select \(PBStrings.AddressTypesTableFields.fieldAddressTypeId) from \(PBStrings.TableNames.addressTypesTable) where \(PBStrings.AddressTypesTableFields.fieldAddressTypeName) = ?))"
                for address in contact.addresses {
                    try database.executeUpdate(insertAddressQuery, values: [currentContactId, address.country ?? NSNull(), address.city ?? NSNull(), address.street ?? NSNull(), address.zipCode ?? NSNull(), address.type])
                }
            }
            catch {
                rollback.pointee = true
            }
        }
    }
    
    func deleteContact(withId contactId: Int) {
        let queue = FMDatabaseQueue(path: self.pathToDatabase)
        queue?.inTransaction { database, rollback in
            do {
                let deleteContactQuery = "delete from \(PBStrings.TableNames.contactsTable) where \(PBStrings.ContactsTableFields.fieldContactId) = ?"
                try database.executeUpdate(deleteContactQuery, values: [contactId])
                
                let deletePhoneNumbersOfEditingContact = "delete from \(PBStrings.TableNames.phoneNumbersTable) where \(PBStrings.PhoneNumberTableFields.fieldPhoneNumberContactId) = ?"
                try database.executeUpdate(deletePhoneNumbersOfEditingContact, values: [contactId])
                
                let deleteEmailsOfEditingContact = "delete from \(PBStrings.TableNames.emailsTable) where \(PBStrings.EmailTableFields.fieldEmailContactId) = ?"
                try database.executeUpdate(deleteEmailsOfEditingContact, values: [contactId])
                
                let deleteAddressesOfEditingContact = "delete from \(PBStrings.TableNames.addressesTable) where \(PBStrings.AddressesTableFields.fieldAddressContactId) = ?"
                try database.executeUpdate(deleteAddressesOfEditingContact, values: [contactId])
            }
            catch {
                rollback.pointee = true
            }
            
        }
    }
    
    func editContact(contact: Contact) {
        
        let dateFormatter = DateFormatter()
        var birthdayInMilliseconds: Int?
        dateFormatter.dateFormat = "dd MMM yyyy"
        
        if let birthday = contact.birthDate {
            let birthdayDate = dateFormatter.date(from: birthday)
            birthdayInMilliseconds = Int((birthdayDate?.timeIntervalSince1970)! * 1000)
        }
        
        let queue = FMDatabaseQueue(path: self.pathToDatabase)
        queue?.inTransaction { database, rollback in
            do {
                let updateContactQuery = "update \(PBStrings.TableNames.contactsTable) set \(PBStrings.ContactsTableFields.fieldFirstName) = ?, \(PBStrings.ContactsTableFields.fieldLastName) = ?,  \(PBStrings.ContactsTableFields.fieldCompany) = ?, \(PBStrings.ContactsTableFields.fieldProfileImage) = ?, \(PBStrings.ContactsTableFields.fieldProfileImageThumbnail) = ?, \(PBStrings.ContactsTableFields.fieldUrl) = ?, \(PBStrings.ContactsTableFields.fieldNotes) = ?, \(PBStrings.ContactsTableFields.fieldBirthDate) = ?, \(PBStrings.ContactsTableFields.fieldJobTitle) = ?, \(PBStrings.ContactsTableFields.fieldPrefixId) = ?, \(PBStrings.ContactsTableFields.fieldIsFavourite) = ?, \(PBStrings.ContactsTableFields.fieldIsBlocked) = ? where \(PBStrings.ContactsTableFields.fieldContactId) = ?"
                
                try database.executeUpdate(updateContactQuery, values: [contact.firstName ?? NSNull(), contact.lastName ?? NSNull(), contact.company ?? NSNull(), contact.profileImage ?? NSNull(), contact.profilePictureThumbnail ?? NSNull(), contact.url ?? NSNull(), contact.note ?? NSNull(), birthdayInMilliseconds ?? NSNull(), contact.jobTitle ?? NSNull(), contact.prefix ?? NSNull(), contact.isFavorite, contact.isBlocked, contact.id])
                
                let deletePhoneNumbersOfEditingContact = "delete from \(PBStrings.TableNames.phoneNumbersTable) where \(PBStrings.PhoneNumberTableFields.fieldPhoneNumberContactId) = ?"
                try database.executeUpdate(deletePhoneNumbersOfEditingContact, values: [contact.id])
                
                let deleteEmailsOfEditingContact = "delete from \(PBStrings.TableNames.emailsTable) where \(PBStrings.EmailTableFields.fieldEmailContactId) = ?"
                try database.executeUpdate(deleteEmailsOfEditingContact, values: [contact.id])
                
                let deleteAddressesOfEditingContact = "delete from \(PBStrings.TableNames.addressesTable) where \(PBStrings.AddressesTableFields.fieldAddressContactId) = ?"
                try database.executeUpdate(deleteAddressesOfEditingContact, values: [contact.id])
                
                let insertPhoneNumberQuery = "insert into \(PBStrings.TableNames.phoneNumbersTable) (\(PBStrings.PhoneNumberTableFields.fieldPhoneNumberContactId), \(PBStrings.PhoneNumberTableFields.fieldPhoneNumberType), \(PBStrings.PhoneNumberTableFields.fieldPhoneNumber)) values (?,(select \(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeId) from \(PBStrings.TableNames.phoneNumberTypesTable) where \(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName) = ?),?)"
                for phone in contact.phoneNumbers {
                    try database.executeUpdate(insertPhoneNumberQuery, values: [contact.id, phone.type, phone.number])
                }
                
                let insertEmailQuery = "insert into \(PBStrings.TableNames.emailsTable) (\(PBStrings.EmailTableFields.fieldEmailContactId), \(PBStrings.EmailTableFields.fieldEmail), \(PBStrings.EmailTableFields.fieldEmailType)) values (?,?,(select \(PBStrings.EmailTypesTableFields.fieldEmailTypeId) from \(PBStrings.TableNames.emailTypesTable) where \(PBStrings.EmailTypesTableFields.fieldEmailTypeName) = ?))"
                for email in contact.emails {
                    try database.executeUpdate(insertEmailQuery, values: [contact.id, email.email, email.type])
                }
                
                let insertAddressQuery = "insert into \(PBStrings.TableNames.addressesTable) (\(PBStrings.AddressesTableFields.fieldAddressContactId), \(PBStrings.AddressesTableFields.fieldAddressCountry), \(PBStrings.AddressesTableFields.fieldAddressCity), \(PBStrings.AddressesTableFields.fieldAddressStreet), \(PBStrings.AddressesTableFields.fieldAddressZipCode), \(PBStrings.AddressesTableFields.fieldAddressType)) values (?,?,?,?,?,(select \(PBStrings.AddressTypesTableFields.fieldAddressTypeId) from \(PBStrings.TableNames.addressTypesTable) where \(PBStrings.AddressTypesTableFields.fieldAddressTypeName) = ?))"
                
                for address in contact.addresses {
                    try database.executeUpdate(insertAddressQuery, values: [contact.id, address.country ?? NSNull(), address.city ?? NSNull(), address.street ?? NSNull(), address.zipCode ?? NSNull(), address.type])
                }
            }
            catch {
                rollback.pointee = true
            }
        }
    }
    
    func loadContactsIdsNamesAndPhoneNumbers() -> [Contact] {
        var contactsFullNamesAndIdsArray: [Contact] = []
        if self.openDatabase() {
            let selectContactsFullNameAndIdQuery = "select \(PBStrings.ContactsTableFields.fieldContactId), \(PBStrings.ContactsTableFields.fieldFirstName), \(PBStrings.ContactsTableFields.fieldLastName) from \(PBStrings.TableNames.contactsTable)"
            do {
                let contactNamesAndIds = try self.database.executeQuery(selectContactsFullNameAndIdQuery, values: nil)
                while contactNamesAndIds.next() {
                    let contact = Contact()
                    contact.id = Int(contactNamesAndIds.int(forColumn: "\(PBStrings.ContactsTableFields.fieldContactId)"))
                    contact.firstName = contactNamesAndIds.string(forColumn: "\(PBStrings.ContactsTableFields.fieldFirstName)") ?? ""
                    contact.lastName = contactNamesAndIds.string(forColumn: "\(PBStrings.ContactsTableFields.fieldLastName)") ?? ""
                    let selectContactPhoneNumber = "select \(PBStrings.PhoneNumberTableFields.fieldPhoneNumber) from \(PBStrings.TableNames.phoneNumbersTable) where \(PBStrings.PhoneNumberTableFields.fieldPhoneNumberContactId) = ? limit 1"
                    let phoneNumberQuery = try self.database.executeQuery(selectContactPhoneNumber, values: [contact.id])
                    phoneNumberQuery.next()
                    let phoneNumber = phoneNumberQuery.string(forColumn: "\(PBStrings.PhoneNumberTableFields.fieldPhoneNumber)")
                    if contact.firstName == "" && contact.lastName == ""
                    {
                        contact.fullName = phoneNumber!
                    } else
                    if contact.lastName == "" {
                        contact.fullName = contact.firstName!
                    } else
                    if contact.firstName == "" {
                        contact.fullName = contact.lastName!
                    } else {
                        contact.fullName = contact.firstName! + " " + contact.lastName!
                    }
                    contactsFullNamesAndIdsArray.append(contact)
                }
            }
            catch {
                print(error.localizedDescription)
            }
            self.database.close()
        }
        return contactsFullNamesAndIdsArray
    }
    
    func loadAddressTypes() -> [String] {
        var types = [String]()
        if self.openDatabase() {
            let selectAddressTypesQuery = "select \(PBStrings.AddressTypesTableFields.fieldAddressTypeName) from \(PBStrings.TableNames.addressTypesTable)"
            do {
                let addressTypes = try self.database.executeQuery(selectAddressTypesQuery, values: nil)
                while addressTypes.next() {
                    let type = addressTypes.string(forColumn: "\(PBStrings.AddressTypesTableFields.fieldAddressTypeName)")
                    types.append(type!)
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            self.database.close()
        }
        return types
    }
    
    func loadPhonenumberTypes() -> [String] {
        var types = [String]()
        if self.openDatabase() {
            let selectPhoneTypesQuery = "select \(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName) from \(PBStrings.TableNames.phoneNumberTypesTable)"
            do {
                let phoneTypes = try self.database.executeQuery(selectPhoneTypesQuery, values: nil)
                while phoneTypes.next() {
                    let type = phoneTypes.string(forColumn: "\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName)")
                    types.append(type!)
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            self.database.close()
        }
        return types
    }
    
    func loadEmailTypes() -> [String] {
        var types = [String]()
        if self.openDatabase() {
            let selectEmailTypesQuery = "select \(PBStrings.EmailTypesTableFields.fieldEmailTypeName) from \(PBStrings.TableNames.emailTypesTable)"
            do {
                let emailTypes = try self.database.executeQuery(selectEmailTypesQuery, values: nil)
                while emailTypes.next() {
                    let type = emailTypes.string(forColumn: "\(PBStrings.EmailTypesTableFields.fieldEmailTypeName)")
                    types.append(type!)
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            self.database.close()
        }
        return types
    }
    
    func loadContact(withId contactId: Int) -> Contact {
        var contact = Contact()
        
        if self.openDatabase() {
            let selectContactsQuery = "select * from \(PBStrings.TableNames.contactsTable) where \(PBStrings.ContactsTableFields.fieldContactId) = ?"
            do {
                let contactProperties = try database.executeQuery(selectContactsQuery, values: [contactId])
                
                let selectEmailsAndTheirTypesForContactQuery = "select \(PBStrings.TableNames.emailsTable).\(PBStrings.EmailTableFields.fieldEmail), \(PBStrings.TableNames.emailTypesTable).\(PBStrings.EmailTypesTableFields.fieldEmailTypeName) from \(PBStrings.TableNames.emailTypesTable) join \(PBStrings.TableNames.emailsTable) on \(PBStrings.TableNames.emailTypesTable).\(PBStrings.EmailTypesTableFields.fieldEmailTypeId) = \(PBStrings.TableNames.emailsTable).\(PBStrings.EmailTableFields.fieldEmailType) where \(PBStrings.TableNames.emailsTable).\(PBStrings.EmailTableFields.fieldEmailContactId) = ?"
                let emailProperties = try self.database.executeQuery(selectEmailsAndTheirTypesForContactQuery, values: [contactId])

                let selectPhoneNumbersAndTheirTypesForContactQuery = "select \(PBStrings.TableNames.phoneNumbersTable).\(PBStrings.PhoneNumberTableFields.fieldPhoneNumber), \(PBStrings.TableNames.phoneNumberTypesTable).\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName) from \(PBStrings.TableNames.phoneNumberTypesTable) join \(PBStrings.TableNames.phoneNumbersTable) on \(PBStrings.TableNames.phoneNumberTypesTable).\(PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeId) = \(PBStrings.TableNames.phoneNumbersTable).\(PBStrings.PhoneNumberTableFields.fieldPhoneNumberType) where \(PBStrings.TableNames.phoneNumbersTable).\(PBStrings.PhoneNumberTableFields.fieldPhoneNumberContactId) = ?"
                let phoneNumberProperties = try self.database.executeQuery(selectPhoneNumbersAndTheirTypesForContactQuery, values: [contactId])

                let selectAddressesForContactQuery = "select \(PBStrings.TableNames.addressesTable).\(PBStrings.AddressesTableFields.fieldAddressCountry), \(PBStrings.TableNames.addressesTable).\(PBStrings.AddressesTableFields.fieldAddressCity), \(PBStrings.TableNames.addressesTable).\(PBStrings.AddressesTableFields.fieldAddressStreet), \(PBStrings.TableNames.addressesTable).\(PBStrings.AddressesTableFields.fieldAddressZipCode), \(PBStrings.TableNames.addressTypesTable).\(PBStrings.AddressTypesTableFields.fieldAddressTypeName) from \(PBStrings.TableNames.addressTypesTable) join \(PBStrings.TableNames.addressesTable) on \(PBStrings.TableNames.addressTypesTable).\(PBStrings.AddressTypesTableFields.fieldAddressTypeId) = \(PBStrings.TableNames.addressesTable).\(PBStrings.AddressesTableFields.fieldAddressType) where \(PBStrings.AddressesTableFields.fieldAddressContactId) = ?"
                let addressProperties = try self.database.executeQuery(selectAddressesForContactQuery, values: [contactId])
                
                contact = Contact.init(with: contactProperties, phoneNumbers: phoneNumberProperties, emails: emailProperties, andAdresses: addressProperties)
            }
            catch {
                print(error.localizedDescription)
            }
            
        self.database.close()
        }
        
    return contact
    }
}
