//
//  AddAddressTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 11/1/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class EditAddressPropertyTableViewCell: UITableViewCell {
    
    
    // MARK: Outlets

    @IBOutlet weak var addressTypeTextFIeld : UITextField!
    
    @IBOutlet weak var streetTextField      : UITextField!
    @IBOutlet weak var cityTextField        : UITextField!
    @IBOutlet weak var countryTextField     : UITextField!
    @IBOutlet weak var zipCodeTextField     : UITextField!
    
    
    // MARK: Cell Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.textFieldsAppearance()
        
        self.createTypePicker()
        self.createToolBarForPickerView()
    }
    
    
    // Public Methods
    
    func setupTags(indexPath: IndexPath) {
        self.addressTypeTextFIeld.tag = indexPath.row
        self.streetTextField     .tag = indexPath.row
        self.cityTextField       .tag = indexPath.row
        self.countryTextField    .tag = indexPath.row
        self.zipCodeTextField    .tag = indexPath.row
    }
    
    
    // Private Methods
    
    private func textFieldsAppearance() {
        self.streetTextField .setLeftBorderAndPadding()
        self.cityTextField   .setLeftBorderAndPadding()
        self.countryTextField.setLeftBorderAndPadding()
        self.zipCodeTextField.setLeftBorderAndPadding()
    }
    
    private func createToolBarForPickerView() {
        let toolBar = PBPickerViewToolbar(doneAction: #selector(self.addressTypeTextFIeld.endEditing), presenter: self)
        self.addressTypeTextFIeld.inputAccessoryView = toolBar
    }
    
    private func createTypePicker() {
        let typePicker = PBTypePickerView(type: .address)
        typePicker.customDelegate = self
        
        self.addressTypeTextFIeld.inputView = typePicker
    }
}

extension EditAddressPropertyTableViewCell: PickerViewSelectedTypeDelegate {
    
    // MARK: Picker View Selected Type Delegate
    
    func didSelectType(type: String) {
        self.addressTypeTextFIeld.text = type
    }
}
