//
//  Address.swift
//  Phonebook
//
//  Created by MacUSER on 10/18/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class Address: NSObject {
    var country : String?
    var city    : String?
    var street  : String?
    var type    : String
    var zipCode : String?
    
    init(country: String?, city: String?, street: String?, zipCode: String?, type: String) {
        self.country = country
        self.city    = city
        self.street  = street
        self.zipCode = zipCode
        self.type    = type
    }
}
