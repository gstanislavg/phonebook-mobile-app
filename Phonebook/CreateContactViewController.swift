//
//  CreateContactViewController.swift
//  Phonebook
//
//  Created by MacUSER on 10/31/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit


// MARK: Enums

enum AddPropertyTypes: Int {
    case phoneNumber = 0
    case email
    case url
    case address
    case birthdate
    case notes
    case count
}

class CreateContactViewController: UIViewController, UIGestureRecognizerDelegate {
    
    
    // MARK: Outlets

    @IBOutlet weak var createNewContactTableView   : UITableView!
    
    @IBOutlet weak var profileImageView            : UIImageView!
    @IBOutlet weak var firstNameTextField          : UITextField!
    @IBOutlet weak var lastNameTextField           : UITextField!
    @IBOutlet weak var jobTitleTextField           : UITextField!
    @IBOutlet weak var companyTextField            : UITextField!
    
    @IBOutlet weak var cancelBarButton             : UIBarButtonItem!
    @IBOutlet weak var doneBarButton               : UIBarButtonItem!
    @IBOutlet weak var deleteContactButton         : UIButton!
    
    @IBOutlet weak var addContactProfileImageLabel : UILabel!
    @IBOutlet weak var deleteContactFooterView     : UIView!
    
    
    // MARK: Constants
    
    private let addPropertyCellIdentifier           = "addPropertyCell"
    private let addNotesCellIdentifier              = "addNotesCell"
    private let editPhonenumberCellIdentifier       = "editPhonenumberCell"
    private let editAddressPropertiesCellIdentifier = "addressPropertiesCell"
    private let editBirthdatCellIdentifier          = "editBirthdayCell"
    private let editEmailCellItentifier             = "editEmailCell"
    private let editUrlCellIdentifier               = "editUrlCell"
    
    private let goBackToContactsListSegueIdentifier = "goBackToContactsViewController"
    
    private let imagePickerController = UIImagePickerController()
    
    private let noteTextViewHeight : CGFloat = 60
    private let sectionHeaderHeight: CGFloat = 50
    
    private let addPropertyCell     = 1
    
    
    // MARK: Variables
    
    private var phoneNumberRowCount = 0
    private var emailRowCount       = 0
    private var urlRowCount         = 0
    private var addressRowCount     = 0
    private var birthdayRowCount    = 0
    
    static var phoneNumberTypes : [String] = []
    static var emailTypes       : [String] = []
    static var addressTypes     : [String] = []
    
    var contact = Contact()
    var isEditingContact = false
    
    
    // MARK: Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customInit()
        self.setupTextFields()
        self.loadProperyTypes()
        
        self.initRowCounts()
        
        if self.isEditingContact == true {
            self.setData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if self.profileImageView.image != #imageLiteral(resourceName: "def-contact-image") {
            self.addContactProfileImageLabel.isHidden = true
        }
    }
    
    
    // MARK: Actions
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteContact(_ sender: Any) {
        let alertController = UIAlertController(title: PBStrings.Titles.attention, message: PBStrings.Messages.deleteContact, preferredStyle: .alert)
        
        let deleteAction = UIAlertAction(title: PBStrings.Titles.deleteContact, style: .destructive) { (action:UIAlertAction) in
            let db = DataBaseManager.sharedInstance
            db.deleteContact(withId: self.contact.id)
            self.performSegue(withIdentifier: self.goBackToContactsListSegueIdentifier, sender: nil)
        }
        
        let cancelAction = UIAlertAction(title: PBStrings.Titles.cancel, style: .default) { (action:UIAlertAction) in
        }
        
        
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.view.endEditing(true)
        
        let db = DataBaseManager.sharedInstance
        
        //        let profileImage = self.profileImageView.image == #imageLiteral(resourceName: "def-contact-image")        ? nil : self.profileImageView.image
        //        let firstName    = (self.firstNameTextField.text?.isEmpty)! ? nil : self.firstNameTextField.text
        //        let lastName     = (self.lastNameTextField.text?.isEmpty)!  ? nil : self.lastNameTextField.text
        //        let company      = (self.companyTextField.text?.isEmpty)!   ? nil : self.companyTextField.text // let company = self.companyTextField.text! - za vsichki
        //        let jobTitle     = (self.jobTitleTextField.text?.isEmpty)!  ? nil : jobTitleTextField.text
        //
        //        self.contact.profileImage = profileImage
        //        self.contact.firstName    = firstName
        //        self.contact.lastName     = lastName
        //        self.contact.company      = company
        //        self.contact.jobTitle     = jobTitle
        
        self.setContactData()
        
        self.isEditingContact ? db.editContact(contact: self.contact) : db.insertContact(contact: self.contact)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addProfilePicture(_ sender: Any) {
        self.imagePickerController.allowsEditing = false
        self.imagePickerController.sourceType    = .photoLibrary
        
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    
    // MARK: Private Methods
    
    private func initRowCounts() {
        self.phoneNumberRowCount = self.contact.phoneNumbers.count
        self.emailRowCount       = self.contact.emails.count
        self.addressRowCount     = self.contact.addresses.count
    }
    
    private func setData() {
        self.firstNameTextField.text = self.contact.firstName
        self.lastNameTextField .text = self.contact.lastName
        self.jobTitleTextField .text = self.contact.jobTitle
        self.companyTextField  .text = self.contact.company
        
        self.profileImageView .image = self.contact.profileImage ?? #imageLiteral(resourceName: "def-contact-image")
        
        if contact.url != nil {
            self.urlRowCount = 1
        }
        
        if contact.birthDate != nil {
            self.birthdayRowCount = 1
        }
        
        self.deleteContactFooterView.isHidden = false
    }
    
    private func customInit() {
        self.createNewContactTableView.isEditing = true
        self.createNewContactTableView.allowsSelectionDuringEditing = true
        
        self.doneBarButton.isEnabled = false
        
        self.deleteContactFooterView.isHidden = true
        
        self.imagePickerController.delegate = self
        
        self.profileImageView.roundImageView()
        self.profileImageView.image = #imageLiteral(resourceName: "def-contact-image")
    }
    
    private func loadProperyTypes() {
        let db = DataBaseManager.sharedInstance
        
        CreateContactViewController.phoneNumberTypes = db.loadPhonenumberTypes()
        CreateContactViewController.emailTypes       = db.loadEmailTypes()
        CreateContactViewController.addressTypes     = db.loadAddressTypes()
    }
    
    private func setupTextFields() {
        self.firstNameTextField.delegate = self
        self.lastNameTextField .delegate = self
        self.jobTitleTextField .delegate = self
        self.companyTextField  .delegate = self
        
        self.firstNameTextField.setBottomBorder()
        self.lastNameTextField .setBottomBorder()
        self.jobTitleTextField .setBottomBorder()
        self.companyTextField  .setBottomBorder()
    }
    
    private func setContactData() {
        self.contact.profileImage = self.profileImageView  .image == #imageLiteral(resourceName: "def-contact-image") ? nil : self.profileImageView.image
        self.contact.firstName    = self.firstNameTextField.text
        self.contact.lastName     = self.lastNameTextField .text
        self.contact.company      = self.companyTextField  .text
        self.contact.jobTitle     = self.jobTitleTextField .text
        
        if self.contact.url == "" {
            self.contact.url = nil
        }
        
        self.contact.emails       = contact.emails      .filter() {$0.email != ""}
        self.contact.phoneNumbers = contact.phoneNumbers.filter() {$0.number != ""}
        self.contact.addresses    = contact.addresses   .filter() {$0.street != "" || $0.city != "" || $0.country != "" || $0.zipCode != ""}
    }
    
    private func createAddPropertyCell(type: String, tableView: UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.addPropertyCellIdentifier) as? AddPropertyTableViewCell {
            cell.propertyTypeLabel.text = type
            return cell
        }
        return UITableViewCell()
    }
    
    private func insertPropertyCell(indexPath: IndexPath) {
        switch indexPath.section {
        case AddPropertyTypes.phoneNumber.rawValue:
            self.contact.phoneNumbers.append(PhoneNumber(type: PBStrings.DefaultTypes.home, number: ""))
            self.phoneNumberRowCount += 1
            
        case AddPropertyTypes.email.rawValue:
            self.contact.emails.append(Email(type: PBStrings.DefaultTypes.home, email: ""))
            self.emailRowCount += 1
            
        case AddPropertyTypes.url.rawValue:
            if self.urlRowCount != 0 {
                return
            }
            
            self.urlRowCount += 1
            
        case AddPropertyTypes.address.rawValue:
            self.contact.addresses.append(Address(country: "", city: "", street: "", zipCode: "", type: PBStrings.DefaultTypes.home))
            self.addressRowCount += 1
            
        case AddPropertyTypes.birthdate.rawValue:
            if self.birthdayRowCount != 0 {
                return
            }
            
            self.birthdayRowCount += 1
        default:
            break
        }
        
        self.createNewContactTableView.beginUpdates()
        self.createNewContactTableView.insertRows(at: [indexPath], with: .automatic)
        self.createNewContactTableView.endUpdates()
    }
    
    private func deletePropertyCell(indexPath: IndexPath) {
        self.createNewContactTableView.beginUpdates()
        switch indexPath.section {
        case AddPropertyTypes.phoneNumber.rawValue:
            self.phoneNumberRowCount -= 1
            self.contact.phoneNumbers.remove(at: indexPath.row)
            
        case AddPropertyTypes.email.rawValue:
            self.emailRowCount -= 1
            self.contact.emails.remove(at: indexPath.row)
            
        case AddPropertyTypes.url.rawValue:
            self.urlRowCount -= 1
            
        case AddPropertyTypes.address.rawValue:
            self.addressRowCount -= 1
            self.contact.addresses.remove(at: indexPath.row)
            
        case AddPropertyTypes.birthdate.rawValue:
            self.birthdayRowCount -= 1
            
        default:
            break
        }
        
        self.createNewContactTableView.deleteRows(at: [indexPath], with: .automatic)
        self.createNewContactTableView.endUpdates()
        
        self.doneBarButton.isEnabled = self.isEditingContact
    }

}

extension CreateContactViewController: UITableViewDataSource {
    
    // MARK: Table View Data Source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return AddPropertyTypes.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = AddPropertyTypes(rawValue: section) else {
            return 0
        }
        switch section {
        case .phoneNumber:
            return self.addPropertyCell + self.phoneNumberRowCount
        case .email:
            return self.addPropertyCell + self.emailRowCount
        case .address:
            return self.addPropertyCell + self.addressRowCount
        case .url:
            return self.addPropertyCell + self.urlRowCount
        case .birthdate:
            return self.addPropertyCell + self.birthdayRowCount
        case .notes:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case AddPropertyTypes.phoneNumber.rawValue:
            if indexPath.row == self.phoneNumberRowCount {
                return self.createAddPropertyCell(type: PBStrings.AppStrings.addPhone, tableView: tableView)
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: self.editPhonenumberCellIdentifier) as? EditPhoneNumberTableViewCell {
                    cell.phoneNumberTypeTextField.delegate = self
                    cell.addPhoneNumberTextField .delegate = self
                    cell.setupTags(indexPath: indexPath)
                    cell.addPhoneNumberTextField .text = self.contact.phoneNumbers[indexPath.row].number
                    cell.phoneNumberTypeTextField.text = self.contact.phoneNumbers[indexPath.row].type
                    cell.addPhoneNumberTextField.becomeFirstResponder()
                    return cell
                }
            }
            
        case AddPropertyTypes.email.rawValue:
            if indexPath.row == self.emailRowCount {
                return self.createAddPropertyCell(type: PBStrings.AppStrings.addEmail, tableView: tableView)
            }
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: editEmailCellItentifier) as? EditEmailTableViewCell {
                cell.addEmailTextField .delegate = self
                cell.emailTypeTextField.delegate = self
                cell.setupTags(indexPath: indexPath)
                cell.addEmailTextField .text = self.contact.emails[indexPath.row].email
                cell.emailTypeTextField.text = self.contact.emails[indexPath.row].type
                cell.addEmailTextField.becomeFirstResponder()
                return cell
            }
            
            
        case AddPropertyTypes.url.rawValue:
            if indexPath.row == self.urlRowCount {
                return self.createAddPropertyCell(type: PBStrings.AppStrings.addUrl, tableView: tableView)
            }
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: self.editUrlCellIdentifier) as? EditUrlTableViewCell {
                cell.addUrlTextField.delegate = self
                cell.addUrlTextField.tag = indexPath.row
                cell.addUrlTextField.text = self.contact.url
                cell.addUrlTextField.becomeFirstResponder()
                cell.urlTypeTextField.text = PBStrings.AppStrings.work
                return cell
            }
            
        case AddPropertyTypes.address.rawValue:
            if indexPath.row == self.addressRowCount {
                return self.createAddPropertyCell(type: PBStrings.AppStrings.addAddress, tableView: tableView)
            }
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: editAddressPropertiesCellIdentifier) as? EditAddressPropertyTableViewCell {
                cell.cityTextField       .delegate = self
                cell.countryTextField    .delegate = self
                cell.zipCodeTextField    .delegate = self
                cell.streetTextField     .delegate = self
                cell.addressTypeTextFIeld.delegate = self
                cell.setupTags(indexPath: indexPath)
                cell.cityTextField       .text = self.contact.addresses[indexPath.row].city
                cell.countryTextField    .text = self.contact.addresses[indexPath.row].country
                cell.zipCodeTextField    .text = self.contact.addresses[indexPath.row].zipCode
                cell.streetTextField     .text = self.contact.addresses[indexPath.row].street
                cell.addressTypeTextFIeld.text = self.contact.addresses[indexPath.row].type
                cell.streetTextField.becomeFirstResponder()
                return cell
            }
            
        case AddPropertyTypes.birthdate.rawValue:
            if indexPath.row == self.birthdayRowCount {
                return self.createAddPropertyCell(type: PBStrings.AppStrings.addBirthday, tableView: tableView)
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: self.editBirthdatCellIdentifier) as? EditBirthdayTableViewCell {
                    cell.datePickerTextField.delegate = self
                    cell.datePickerTextField.tag = indexPath.row
                    cell.datePickerTextField.text = self.contact.birthDate
                    return cell
                }
            }
            
        case AddPropertyTypes.notes.rawValue:
            if let cell = tableView.dequeueReusableCell(withIdentifier: self.addNotesCellIdentifier) as? NoteTableViewCell {
                cell.notesTextView.delegate = self
                cell.notesTextView.tag = indexPath.row
                cell.notesTextView.text = self.contact.note
                return cell
            }
        default:
            return UITableViewCell()
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == AddPropertyTypes.notes.rawValue ? false : true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            switch indexPath.section {
            case AddPropertyTypes.phoneNumber.rawValue:
                self.deletePropertyCell(indexPath: indexPath)
                
            case AddPropertyTypes.email.rawValue:
                self.deletePropertyCell(indexPath: indexPath)
                
            case AddPropertyTypes.url.rawValue:
                self.deletePropertyCell(indexPath: indexPath)
                
            case AddPropertyTypes.address.rawValue:
                self.deletePropertyCell(indexPath: indexPath)
                
            case AddPropertyTypes.birthdate.rawValue:
                self.deletePropertyCell(indexPath: indexPath)
                
            default:
                break
            }
        } else if editingStyle == .insert {
            switch indexPath.section {
            case AddPropertyTypes.phoneNumber.rawValue:
                self.insertPropertyCell(indexPath: indexPath)
                
            case AddPropertyTypes.email.rawValue:
                self.insertPropertyCell(indexPath: indexPath)
                
            case AddPropertyTypes.url.rawValue:
                self.insertPropertyCell(indexPath: indexPath)
                
            case AddPropertyTypes.address.rawValue:
                self.insertPropertyCell(indexPath: indexPath)
                
            case AddPropertyTypes.birthdate.rawValue:
                self.insertPropertyCell(indexPath: indexPath)
                
            default:
                break
            }
        }
    }
    
}

extension CreateContactViewController: UITableViewDelegate {
    
    // MARK: Table View Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case AddPropertyTypes.phoneNumber.rawValue:
            if indexPath.row == self.phoneNumberRowCount {
                self.insertPropertyCell(indexPath: indexPath)
            }
        case AddPropertyTypes.email.rawValue:
            if indexPath.row == self.emailRowCount {
                self.insertPropertyCell(indexPath: indexPath)
            }
        case AddPropertyTypes.url.rawValue:
            if indexPath.row == self.urlRowCount {
                self.insertPropertyCell(indexPath: indexPath)
            }
        case AddPropertyTypes.address.rawValue:
            if indexPath.row == self.addressRowCount {
                self.insertPropertyCell(indexPath: indexPath)
            }
        case AddPropertyTypes.birthdate.rawValue:
            if indexPath.row == self.birthdayRowCount {
                self.insertPropertyCell(indexPath: indexPath)
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == AddPropertyTypes.notes.rawValue {
            return noteTextViewHeight
        }
        
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let whiteHeaderView = UIView()
        whiteHeaderView.backgroundColor = .white
        
        return whiteHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.sectionHeaderHeight
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        switch indexPath.section {
        case AddPropertyTypes.phoneNumber.rawValue:
            if indexPath.row == self.phoneNumberRowCount {
                return .insert
            }
            
            return .delete
            
        case AddPropertyTypes.email.rawValue:
            if indexPath.row == self.emailRowCount {
                return .insert
            }
            
            return .delete
            
        case AddPropertyTypes.url.rawValue:
            if indexPath.row == self.urlRowCount {
                return .insert
            }
            
            return .delete
            
        case AddPropertyTypes.address.rawValue:
            if indexPath.row == self.addressRowCount {
                return .insert
            }
            
            return .delete
            
        case AddPropertyTypes.birthdate.rawValue:
            if indexPath.row == self.birthdayRowCount {
                return .insert
            }
            
            return .delete
            
        default:
            break
        }
        return .none
    }

}

extension CreateContactViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        self.doneBarButton.isEnabled = !newString.isEmpty
        
        let currentPhoneNumberIndexPath = IndexPath(row: textField.tag, section: AddPropertyTypes.phoneNumber.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentPhoneNumberIndexPath) as? EditPhoneNumberTableViewCell {
            if textField == cell.phoneNumberTypeTextField {
                return false
            }
        }
        
        let currentEmailIndexPath = IndexPath(row: textField.tag, section: AddPropertyTypes.email.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentEmailIndexPath) as? EditEmailTableViewCell {
            if textField == cell.emailTypeTextField {
                return false
            }
        }
        
        let currentAddressIndexPath = IndexPath(row: textField.tag, section: AddPropertyTypes.address.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentAddressIndexPath) as? EditAddressPropertyTableViewCell {
            if textField == cell.addressTypeTextFIeld {
                return false
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let currentPhoneNumberIndexPath = IndexPath(row: textField.tag, section: AddPropertyTypes.phoneNumber.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentPhoneNumberIndexPath) as? EditPhoneNumberTableViewCell {
            self.contact.phoneNumbers[currentPhoneNumberIndexPath.row].number = cell.addPhoneNumberTextField .text ?? ""
            self.contact.phoneNumbers[currentPhoneNumberIndexPath.row].type   = cell.phoneNumberTypeTextField.text ?? PBStrings.DefaultTypes.mobile
        }
        
        let currentEmailIndexPath = IndexPath(row: textField.tag, section: AddPropertyTypes.email.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentEmailIndexPath) as? EditEmailTableViewCell {
            self.contact.emails[currentEmailIndexPath.row].email = cell.addEmailTextField .text ?? ""
            self.contact.emails[currentEmailIndexPath.row].type  = cell.emailTypeTextField.text ?? PBStrings.DefaultTypes.home
        }
        
        let currentAddressIndexPath = IndexPath(row: textField.tag, section: AddPropertyTypes.address.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentAddressIndexPath) as? EditAddressPropertyTableViewCell {
            self.contact.addresses[currentAddressIndexPath.row].street  = cell.streetTextField     .text
            self.contact.addresses[currentAddressIndexPath.row].city    = cell.cityTextField       .text
            self.contact.addresses[currentAddressIndexPath.row].country = cell.countryTextField    .text
            self.contact.addresses[currentAddressIndexPath.row].zipCode = cell.zipCodeTextField    .text
            self.contact.addresses[currentAddressIndexPath.row].type    = cell.addressTypeTextFIeld.text ?? PBStrings.DefaultTypes.home
        }
        
        let currentUrlIndexPath = IndexPath(row: textField.tag, section: AddPropertyTypes.url.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentUrlIndexPath) as? EditUrlTableViewCell {
            self.contact.url = cell.addUrlTextField.text
        }
        
        let currentBirthdayIndexPath = IndexPath(row: textField.tag, section: AddPropertyTypes.birthdate.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentBirthdayIndexPath) as? EditBirthdayTableViewCell {
            self.contact.birthDate = cell.datePickerTextField.text
        }
    }
}

extension CreateContactViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let currentOffset = self.createNewContactTableView.contentOffset
        UIView.setAnimationsEnabled(false)
        self.createNewContactTableView.beginUpdates()
        self.createNewContactTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        self.createNewContactTableView.setContentOffset(currentOffset, animated: false)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        let currentNoteIndexPath = IndexPath(row: textView.tag, section: AddPropertyTypes.notes.rawValue)
        
        if let cell = self.createNewContactTableView.cellForRow(at: currentNoteIndexPath) as? NoteTableViewCell {
            self.contact.note = cell.notesTextView.text
        }
    }
}

extension CreateContactViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.profileImageView.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

