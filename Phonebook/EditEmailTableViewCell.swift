//
//  EditEmailTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 11/6/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class EditEmailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var emailTypeTextField : UITextField!
    @IBOutlet weak var addEmailTextField  : UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.addEmailTextField.setLeftBorderAndPadding()
        self.addEmailTextField.keyboardType = UIKeyboardType.emailAddress
        
        self.createTypePicker()
        self.createToolBarForPickerView()
    }
    
    
    // MARK: Public Methods
    
    func setupTags(indexPath: IndexPath) {
        self.addEmailTextField .tag = indexPath.row
        self.emailTypeTextField.tag = indexPath.row
    }
    
    
    // MARK: Private Methods
    
    private func createToolBarForPickerView() {
        let toolBar = PBPickerViewToolbar(doneAction: #selector(self.emailTypeTextField.endEditing), presenter: self)
        
        self.emailTypeTextField.inputAccessoryView = toolBar
    }
    
    private func createTypePicker() {
        let typePicker = PBTypePickerView(type: .email)
        typePicker.customDelegate = self
    
        self.emailTypeTextField.inputView = typePicker
    }
}

extension EditEmailTableViewCell: PickerViewSelectedTypeDelegate {
    
    // MARK: Picker View Selected Type Delegate
    
    func didSelectType(type: String) {
        self.emailTypeTextField.text = type
    }
}
