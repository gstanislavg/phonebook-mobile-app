//
//  CustomTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 10/6/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    
    // MARK: Outlets

    @IBOutlet weak var contactNameLabel: UILabel!
    
    
    // MARK: Public Methods
    
    func setNameOf(contact contact: Contact) {
        self.contactNameLabel.text = contact.fullName
        self.contactNameLabel.sizeToFit()
    }
}
