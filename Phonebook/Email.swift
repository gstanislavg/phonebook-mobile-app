//
//  Email.swift
//  Phonebook
//
//  Created by MacUSER on 10/19/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class Email: NSObject {
    var type  : String
    var email : String
    
    init(type: String, email: String) {
        self.type  = type
        self.email = email
    }
}
