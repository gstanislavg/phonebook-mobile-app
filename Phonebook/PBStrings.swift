//
//  PBStrings.swift
//  Phonebook
//
//  Created by Stanislav Georgiev on 17.12.18.
//  Copyright © 2018 MacUSER. All rights reserved.
//

import Foundation

struct PBStrings {
    
    // MARK: Database Strings
    
    struct DefaultTypes {
        static let home   = "home"
        static let mobile = "mobile"
        static let work   = "work"
        static let other  = "other"
    }
    
    struct TableNames {
        static let contactsTable         = "contacts"
        static let phoneNumbersTable     = "phoneNumbers"
        static let phoneNumberTypesTable = "phoneTypes"
        static let emailsTable           = "emails"
        static let emailTypesTable       = "emailTypes"
        static let addressesTable        = "addresses"
        static let addressTypesTable     = "addressTypes"
    }
    
    struct ContactsTableFields {
        static let fieldContactId             = "id"
        static let fieldDateAdded             = "dateAdded"
        static let fieldFirstName             = "firstName"
        static let fieldLastName              = "lastName"
        static let fieldCountryId             = "countryId"
        static let fieldCity                  = "city"
        static let fieldAddress               = "address"
        static let fieldZipCode               = "zipCode"
        static let fieldCompany               = "company"
        static let fieldProfileImage          = "profileImage"
        static let fieldProfileImageThumbnail = "profileImageThumbnail"
        static let fieldUrl                   = "url"
        static let fieldNotes                 = "notes"
        static let fieldBirthDate             = "birthDate"
        static let fieldJobTitle              = "jobTitle"
        static let fieldPrefixId              = "prefixId"
        static let fieldIsFavourite           = "isFavourite"
        static let fieldIsBlocked             = "isBlocked"
    }
    
    struct PhoneNumberTableFields {
        static let fieldPhoneNumberId = "id"
        static let fieldPhoneNumberContactId = "contactId"
        static let fieldPhoneNumberType = "type"
        static let fieldPhoneNumber = "phoneNumber"
    }
    
    struct PhoneNumberTypesTableFields {
        static let fieldPhoneNumberTypeId   = "id"
        static let fieldPhoneNumberTypeName = "name"
    }
    
    struct EmailTableFields {
        static let fieldEmailId        = "id"
        static let fieldEmailContactId = "contactId"
        static let fieldEmail          = "mail"
        static let fieldEmailType      = "type"
    }
    
    struct EmailTypesTableFields {
        static let fieldEmailTypeId   = "id"
        static let fieldEmailTypeName = "name"
    }
    
    struct AddressesTableFields {
        static let fieldAddressId        = "id"
        static let fieldAddressContactId = "contactId"
        static let fieldAddressCountry   = "country"
        static let fieldAddressCity      = "city"
        static let fieldAddressStreet    = "street"
        static let fieldAddressZipCode   = "zipcode"
        static let fieldAddressType      = "type"
    }
    
    struct AddressTypesTableFields {
        static let fieldAddressTypeId   = "id"
        static let fieldAddressTypeName = "name"
    }
    
    
    // MARK: App Strings
    
    struct AppStrings {
        static let all = "All"
        static let url = "url"
        static let work = "work"
        static let dateAdded   = "date added"
        static let birthday    = "birthday"
        static let addBirthday = "Add birthday"
        static let addAddress  = "Add address"
        static let addUrl      = "Add url"
        static let addEmail    = "Add email"
        static let addPhone    = "Add phone"
    }
    
    struct Titles {
        static let attention     = "Attention!"
        static let deleteContact = "Delete Contact"
        static let cancel        = "Cancel"
    }
    
    struct Messages {
        static let deleteContact = "Are you sure you want to delete the contact?"
    }
    
    struct DateFormat {
        static let `default` = "dd MMM yyyy"
    }
    
}
