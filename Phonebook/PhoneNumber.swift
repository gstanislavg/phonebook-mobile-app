//
//  PhoneNumbers.swift
//  Phonebook
//
//  Created by MacUSER on 10/19/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class PhoneNumber: NSObject {
    var type   : String
    var number : String
    
    init(type: String, number: String) {
        self.type   = type
        self.number = number
    }
}
