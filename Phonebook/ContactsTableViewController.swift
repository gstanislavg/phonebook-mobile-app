//
//  ContactsTableViewController.swift
//  Phonebook
//
//  Created by MacUSER on 10/6/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit
import Foundation

class ContactsTableViewController: UIViewController {
    
    
    // MARK: Outlets
    
    @IBOutlet weak var contactsTableView       : UITableView!
    
    @IBOutlet weak var myContactProfileView    : UIView!
    @IBOutlet weak var myContactProfilePicture : UIImageView!
    @IBOutlet weak var myContactProfileName    : UILabel!
    
    
    // MARK: Unwind Segue
    
    @IBAction func unwindToVC1(segue: UIStoryboardSegue) { }
    
    
    // MARK: Constants
    
    private let searchController = UISearchController(searchResultsController: nil) // Inicializaciq na searchcontroller-a
    
    private let indexSectionArray: [String] = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
    private let goToContactProfileSegueIdentifier = "showContactProfileViewController"
    private let contactNameCellIdentifier         = "contactNameCell"
    
    
    // MARK: Variables
    
    private var contactsArray: [Contact]? // Masiv s imena i id-ta na kontakti
    
    private var filteredContactsArray: [Contact] = [] // Masiv s filtriranite kontakti pri Search
    
    private var contactIdToSend: Int? // Id-to na kontakta koito sme izbrali i trqbva da vizualizirame na profil ekrana
    
    private var sortedSectionLetters: [String] = [] // Sortirani purvi simvoli ot imenata na sushtestvuvashti kontakti, za izvejdaneto im, kato imena na sekcii
    private var sections: [[Contact]] = [[]] // Dvumeren masiv, sudurjasht sekcii i prinadlejashtite im kontakti
    
    
    // MARK: Controller Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.prepareContactsToBeDisplayed()
    }
    
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.goToContactProfileSegueIdentifier {
            if let nextVC = segue.destination as? ContactProfileViewController {
                nextVC.selectedContactId = self.contactIdToSend
            }
        }
    }
    
    
    // MARK: Private Methods
    
    private func prepareContactsToBeDisplayed() {
        let myDB = DataBaseManager.sharedInstance
        
        self.contactsArray = myDB.loadContactsIdsNamesAndPhoneNumbers()
        
        let firstLetters = contactsArray?.map {$0.contactFirstLetter}.sorted()
        
        let uniqueFirstLetters = Array(Set(firstLetters ?? [])) // Cast to "Set" is used to remove duplicates
        self.sortedSectionLetters = uniqueFirstLetters.sorted()
        self.sections = sortedSectionLetters.map { firstLetters in
            return (self.contactsArray?.filter { $0.contactFirstLetter == firstLetters }.sorted { $0.fullName < $1.fullName} ?? [Contact]())
        } // Initializes a two dimensional array containing all unique first letters from the names of the contacts. For every first letter, filter the contacts` first letters, find those that match the letter and then sort the matching contacts by fullName.
        
        self.contactsTableView.tableHeaderView = myContactProfileView // Postavqne na View, sudurjashto informaciq za nashiq profil, kato HEADER v tableView
        self.myContactProfilePicture.roundImageView()
        
        self.contactsTableView.reloadData()
    }
    
    private func setupNavBar () {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        self.navigationItem.searchController = self.searchController
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.definesPresentationContext = true
        
        self.navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    private func filterContentForSearchText(_ searchText: String, scope: String = PBStrings.AppStrings.all) {
//        self.filteredContactsArray = (self.contactsArray?.filter({( contact : Contact) -> Bool in
//            return ((contact.fullName).lowercased().contains(searchText.lowercased()))}))!
        
        self.filteredContactsArray = self.contactsArray?.filter({$0.fullName.lowercased().contains(searchText.lowercased())}) ?? [Contact]()
        
        self.contactsTableView.reloadData()
    }
    
    
    // MARK: Helpers
    
    private var isFiltering: Bool {
        return self.searchController.isActive && !self.isSearchBarEmpty
    }
    
    private var isSearchBarEmpty: Bool {
        return self.searchController.searchBar.text?.isEmpty ?? true
    }
    
}

extension ContactsTableViewController: UISearchResultsUpdating {
    
    // MARK: Search Results Updating
    
    func updateSearchResults(for searchController: UISearchController) {
        self.filterContentForSearchText(searchController.searchBar.text!)
    }
}

extension ContactsTableViewController: UITableViewDataSource {
    
    // MARK: Table View Data Source
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.contactNameCellIdentifier, for: indexPath) as! ContactTableViewCell
        self.isFiltering ? cell.setNameOf(contact: self.filteredContactsArray[indexPath.row]) :
                           cell.setNameOf(contact: self.sections[indexPath.section][indexPath.row])
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.isFiltering ? 1 : self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.isFiltering ? self.filteredContactsArray.count :
                                  self.sections[section].count
    }
    
}

extension ContactsTableViewController: UITableViewDelegate {
    
    // MARK: Table View Delegate
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        //        if let index = self.sortedSectionLetters.index(of: title)
        //        {
        //            return index
        //        } else {
        //            return -1
        //        }
        
        if let index = self.sortedSectionLetters.index(of: title) {
            return index
        }
        
        return -1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.isFiltering ? "" : self.sortedSectionLetters[section]
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return self.indexSectionArray
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.contactIdToSend = self.isFiltering ? self.filteredContactsArray[indexPath.row].id :
            self.sections[indexPath.section][indexPath.row].id
        
        self.contactsTableView.deselectRow(at: indexPath, animated: false)
        
        self.performSegue(withIdentifier: self.goToContactProfileSegueIdentifier, sender: self)
    }
}

