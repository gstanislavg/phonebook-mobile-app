//
//  PhoneNumberTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 10/12/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class MutualTableViewCell: UITableViewCell {
    
    
    // MARK: Outlets

    @IBOutlet weak var contentLabel : UILabel!
    @IBOutlet weak var typeLabel    : UILabel!
    
    
    // MARK: Public Methods
    
    func populateMutualCellWith(type: String, andContent content: String?) {
        self.typeLabel   .text = type
        self.contentLabel.text = content
    }
}
