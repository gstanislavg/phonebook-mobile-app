//
//  Contact.swift
//  Phonebook
//
//  Created by MacUSER on 10/9/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class Contact {
    var id                      : Int
    var firstName               : String?
    var lastName                : String?
    var company                 : String?
    var profileImage            : UIImage?
    var profilePictureThumbnail : String?
    var jobTitle                : String?
    var prefix                  : Int?
    var isFavorite              : Bool
    var isBlocked               : Bool
    var phoneNumbers            : [PhoneNumber]
    var emails                  : [Email]
    var dateAdded               : String
    var url                     : String?
    var birthDate               : String?
    var addresses               : [Address]
    var note                    : String?
    var fullName                : String
    
    init() {
        self.id           = 0
        self.fullName     = ""
        self.phoneNumbers = []
        self.emails       = []
        self.dateAdded    = ""
        self.addresses    = []
        self.isBlocked    = false
        self.isFavorite   = false
    }
    
    var contactFirstLetter: String {
        if self.firstName == "" && self.lastName == "" {
            return "#"
        }
        
        if self.lastName == "" {
            return String(self.firstName![self.firstName!.startIndex]).uppercased()
        }
        
        return String(self.lastName![self.lastName!.startIndex]).uppercased()
    }
    
    init(with result: FMResultSet, phoneNumbers: FMResultSet, emails: FMResultSet, andAdresses addresses: FMResultSet) {

        result.next() // get first result
        
        self.id        = Int(result.int(forColumn: PBStrings.ContactsTableFields.fieldContactId))
        self.dateAdded = Int(result.longLongInt(forColumn: PBStrings.ContactsTableFields.fieldDateAdded)).convertMilisecondsToStringDate() ?? ""
        self.firstName = result.string(forColumn: PBStrings.ContactsTableFields.fieldFirstName) ?? ""
        self.lastName  = result.string(forColumn: PBStrings.ContactsTableFields.fieldLastName) ?? ""
        
        self.fullName = self.lastName == "" ? self.firstName! : self.firstName! + " " + self.lastName!
        
//        if self.lastName == "" {
//            self.fullName = self.firstName!
//        } else {
//            self.fullName = self.firstName! + " " + self.lastName!
//        }
        
        self.company = result.string(forColumn: PBStrings.ContactsTableFields.fieldCompany)
        
        if let imageData = result.data(forColumn: PBStrings.ContactsTableFields.fieldProfileImage) {
            profileImage = UIImage(data: imageData)
        }
        
        self.profilePictureThumbnail = String(describing: result.string(forColumn: PBStrings.ContactsTableFields.fieldProfileImageThumbnail))
        self.url        = result.string(forColumn: PBStrings.ContactsTableFields.fieldUrl)
        self.note       = result.string(forColumn: PBStrings.ContactsTableFields.fieldNotes)
        self.birthDate  = Int(result.longLongInt(forColumn: PBStrings.ContactsTableFields.fieldBirthDate)).convertMilisecondsToStringDate()
        self.jobTitle   = result .string(forColumn: PBStrings.ContactsTableFields.fieldJobTitle)
        self.prefix     = Int(result.int(forColumn: PBStrings.ContactsTableFields.fieldPrefixId))
        self.isFavorite = result   .bool(forColumn: PBStrings.ContactsTableFields.fieldIsFavourite)
        self.isBlocked  = result   .bool(forColumn: PBStrings.ContactsTableFields.fieldIsBlocked)
        
        self.phoneNumbers = []
        
        while phoneNumbers.next() {
            let phoneNumber = PhoneNumber(type   : phoneNumbers.string(forColumn: PBStrings.PhoneNumberTypesTableFields.fieldPhoneNumberTypeName) ?? "",
                                          number : phoneNumbers.string(forColumn: PBStrings.PhoneNumberTableFields.fieldPhoneNumber) ?? "")
            self.phoneNumbers.append(phoneNumber)
        }
        
        self.emails = []
        
        while emails.next() {
            let email = Email(type  : emails.string(forColumn: PBStrings.EmailTypesTableFields.fieldEmailTypeName) ?? "",
                              email : emails.string(forColumn: PBStrings.EmailTableFields.fieldEmail)              ?? "")
            self.emails.append(email)
        }
        
        self.addresses = []
        
        while addresses.next() {
            let address = Address(country : addresses.string(forColumn: PBStrings.AddressesTableFields.fieldAddressCountry)     ?? "",
                                  city    : addresses.string(forColumn: PBStrings.AddressesTableFields.fieldAddressCity)        ?? "",
                                  street  : addresses.string(forColumn: PBStrings.AddressesTableFields.fieldAddressStreet)      ?? "",
                                  zipCode : addresses.string(forColumn: PBStrings.AddressesTableFields.fieldAddressZipCode)     ?? "",
                                  type    : addresses.string(forColumn: PBStrings.AddressTypesTableFields.fieldAddressTypeName) ?? "")
            self.addresses.append(address)
        }
    }
}
