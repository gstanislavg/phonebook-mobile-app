//
//  Extensions.swift
//  Phonebook
//
//  Created by MacUSER on 10/11/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func roundImageView() {
        self.layer.cornerRadius = self.frame.size.height / 2;
        self.clipsToBounds = true;
    }
}

extension Int {
    func convertMilisecondsToStringDate() -> String? {
        if self != 0 {
            let miliseconds = self
            
            let dateVar = Date(timeIntervalSince1970 : TimeInterval(miliseconds/1000))
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            
            return dateFormatter.string(from: dateVar)
        }
        return nil
    }
}

extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 0.0
    }
    
    func setLeftBorderAndPadding() {
        self.borderStyle = .none
        let leftBorder = CALayer()
        leftBorder.frame = CGRect(x: 0, y: 0, width: 1,height: self.frame.size.height)
        leftBorder.backgroundColor = UIColor.lightGray.cgColor
        self.layer.addSublayer(leftBorder)
        
        self.layer.sublayerTransform = CATransform3DMakeTranslation(0, 0, 0)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = UITextFieldViewMode.always
    }
}


