//
//  NoteTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 10/17/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class NoteTableViewCell: UITableViewCell, UITextViewDelegate {
    
    
    // MARK: Outlets

    @IBOutlet weak var notesTextView: UITextView!
    
}
