//
//  EditPropertiesTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 11/1/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class EditPhoneNumberTableViewCell: UITableViewCell {
    
    
    // MARK: Outlets
    
    @IBOutlet weak var phoneNumberTypeTextField : UITextField!
    @IBOutlet weak var addPhoneNumberTextField  : UITextField!
    
    
    // MARK: Cell Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.addPhoneNumberTextField.setLeftBorderAndPadding()
        self.addPhoneNumberTextField.keyboardType = UIKeyboardType.phonePad
        
        self.createTypePicker()
        self.createToolBarForPickerView()
    }
    
    
    // MARK: Public Methods
    
    func setupTags(indexPath: IndexPath) {
        self.addPhoneNumberTextField .tag = indexPath.row
        self.phoneNumberTypeTextField.tag = indexPath.row
    }
    
    
    // MARK: Private Methods
    
    private func createToolBarForPickerView() {
        let toolBar = PBPickerViewToolbar(doneAction: #selector(self.phoneNumberTypeTextField.endEditing), presenter: self)
        
        self.phoneNumberTypeTextField.inputAccessoryView = toolBar
    }
    
    private func createTypePicker() {
        let typePicker = PBTypePickerView(type: .phoneNumber)
        typePicker.customDelegate = self
        
        self.phoneNumberTypeTextField.inputView = typePicker
    }
}

extension EditPhoneNumberTableViewCell: PickerViewSelectedTypeDelegate {
    
    // MARK: Picker View Selected Type Delegate
    
    func didSelectType(type: String) {
        self.phoneNumberTypeTextField.text = type
    }
}

