//
//  AddressTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 10/12/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {
    
    
    // MARK: Outlets

    @IBOutlet weak var streetLabel     : UILabel!
    @IBOutlet weak var titleLabel      : UILabel!
    @IBOutlet weak var zipAndCityLabel : UILabel!
    @IBOutlet weak var countryLabel    : UILabel!
    
    
    // MARK: Public Methods
    
    func populateAddressCellWith(address : Address) {
        self.streetLabel.text = address.street
        
        if let zipCode =  address.zipCode, let city = address.city {
            self.zipAndCityLabel.text = zipCode + " " + city
        }
        
        self.countryLabel.text = address.country
        self.titleLabel  .text = address.type
    }
}
