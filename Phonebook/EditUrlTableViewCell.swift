//
//  EditUrlTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 11/6/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class EditUrlTableViewCell: UITableViewCell {

    @IBOutlet weak var urlTypeTextField : UITextField!
    @IBOutlet weak var addUrlTextField  : UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.addUrlTextField.setLeftBorderAndPadding()
        self.addUrlTextField.keyboardType = UIKeyboardType.URL
    }

}
