//
//  ContactProfileViewController.swift
//  Phonebook
//
//  Created by MacUSER on 10/6/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

enum ContactPropertyTypes: Int {
    case phoneNumber
    case email
    case url
    case address
    case birthdate
    case dateAdded
    case notes
    case count
}

class ContactProfileViewController: UIViewController {
    
    
    // MARK: Outlets
  
    @IBOutlet weak var contactPictureCircleImageView : UIImageView!
    @IBOutlet weak var contactNameLabel              : UILabel!
    @IBOutlet weak var contactJobTitleLabel          : UILabel!
    @IBOutlet weak var contactCompanyLabel           : UILabel!
    @IBOutlet weak var contactProfileTableView       : UITableView!
    @IBOutlet weak var profileView                   : UIView!
    
    
    // MARL: Constants
    
    private let mutualCellIdentifier  = "mutualCell"
    private let addressCellIdentifier = "addressCell"
    private let noteCellIdentifier    = "noteCell"
    
    private let editContactSegueIdentifier = "editContactSegue"
    
    
    // MARK: Variables

    var         selectedContactId : Int!
    private var selectedContact   : Contact!
    
    
    // MARK: Controller Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        self.loadSelectedContact()
    }
    
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.editContactSegueIdentifier {
            if let nextVC = segue.destination as? CreateContactViewController {
                nextVC.contact = self.selectedContact
                nextVC.isEditingContact = true
            }
        }
    }
    
    // MARK: Private Methods
    
    private func loadSelectedContact() {
        self.selectedContact = DataBaseManager.sharedInstance.loadContact(withId: self.selectedContactId)
        
        self.contactPictureCircleImageView.roundImageView()
        self.contactPictureCircleImageView.image = self.selectedContact.profileImage ?? #imageLiteral(resourceName: "def-contact-image")
        
        self.contactNameLabel    .text = self.selectedContact.fullName
        self.contactJobTitleLabel.text = self.selectedContact.jobTitle
        self.contactCompanyLabel .text = self.selectedContact.company
        
        self.contactProfileTableView.reloadData()
    }
    
    private func createAndPopulateMutualCell(type: String, content: String?,tableView: UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.mutualCellIdentifier) as? MutualTableViewCell {
            cell.populateMutualCellWith(type: type, andContent: content)
            return cell
        }
        return UITableViewCell()
    }
    
    private func createAndPopulateNoteCell (content: String?, tableView: UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.noteCellIdentifier) as? NoteTableViewCell {
            cell.notesTextView.text = content
            cell.notesTextView.delegate = self
            return cell
        }
        return UITableViewCell()
    }
    
    private func createAndPopulateAddressCell(address: Address, tableView: UITableView) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.addressCellIdentifier ) as? AddressTableViewCell {
            cell.populateAddressCellWith(address: address)
            return cell
        }
        return UITableViewCell()
    }
    
}

extension ContactProfileViewController: UITableViewDataSource {
    
    // MARK: Table View Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = ContactPropertyTypes(rawValue: section) else {
            return 0
        }
        switch section {
        case .phoneNumber:
            return self.selectedContact.phoneNumbers.count
        case .dateAdded:
            return 1
        case .address:
            return self.selectedContact.addresses.count
        case .notes:
            return 1
        case .url:
            if self.selectedContact.url != nil {
                return 1
            }
            
            return 0
        case .birthdate:
            if self.selectedContact.birthDate != nil {
                return 1
            }
            
            return 0
        case .email:
            return self.selectedContact.emails.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return ContactPropertyTypes.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case ContactPropertyTypes.phoneNumber.rawValue:
            return self.createAndPopulateMutualCell(type: self.selectedContact.phoneNumbers[indexPath.row].type, content: self.selectedContact.phoneNumbers[indexPath.row].number, tableView: tableView)
            
        case ContactPropertyTypes.email.rawValue:
            return self.createAndPopulateMutualCell(type: self.selectedContact.emails[indexPath.row].type, content: self.selectedContact.emails[indexPath.row].email, tableView: tableView)
            
        case ContactPropertyTypes.address.rawValue:
            return self.createAndPopulateAddressCell(address: self.selectedContact.addresses[indexPath.row], tableView: tableView)
            
        case ContactPropertyTypes.notes.rawValue:
            return self.createAndPopulateNoteCell(content: self.selectedContact.note, tableView: tableView)
            
        case ContactPropertyTypes.url.rawValue:
            return self.createAndPopulateMutualCell(type: PBStrings.AppStrings.url,       content: self.selectedContact.url,       tableView: tableView)
            
        case ContactPropertyTypes.dateAdded.rawValue:
            return self.createAndPopulateMutualCell(type: PBStrings.AppStrings.dateAdded, content: self.selectedContact.dateAdded, tableView: tableView)
            
        case ContactPropertyTypes.birthdate.rawValue:
            return self.createAndPopulateMutualCell(type: PBStrings.AppStrings.birthday,  content: self.selectedContact.birthDate, tableView: tableView)
            
        default:
            return UITableViewCell()
        }
    }
    
}

extension ContactProfileViewController: UITableViewDelegate {
    
    // MARK: Table View Delegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

extension ContactProfileViewController: UITextViewDelegate {
    
    // MARK: Text View Delegate
    
    func textViewDidChange(_ textView: UITextView) {
        let currentOffset = self.contactProfileTableView.contentOffset
        UIView.setAnimationsEnabled(false)
        self.contactProfileTableView.beginUpdates()
        self.contactProfileTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        self.contactProfileTableView.setContentOffset(currentOffset, animated: false)
    }
    
}
