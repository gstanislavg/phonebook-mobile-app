//
//  EditBirthdayTableViewCell.swift
//  Phonebook
//
//  Created by MacUSER on 11/2/17.
//  Copyright © 2017 MacUSER. All rights reserved.
//

import UIKit

class EditBirthdayTableViewCell: UITableViewCell {
    
    @IBOutlet weak var datePickerTextField: UITextField!
    
    let datePickerView = UIDatePicker()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.datePickerTextField.setLeftBorderAndPadding()
    }
    
    func displayDatePicker() {
        self.createToolBar()
        self.datePickerView.datePickerMode = .date
        self.datePickerView.maximumDate = Date()

        self.datePickerTextField.inputView = datePickerView
    }
    
    @IBAction func showDatePicker(_ sender: UITextField) {
        self.displayDatePicker()
    }
    
    @objc func cancelButtonAction() {
        self.datePickerTextField.endEditing(true)
    }
    
    func createToolBar() {
//        let toolBar = UIToolbar()
//        toolBar.sizeToFit()
//
//        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.saveDateToTextField))
//        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelButtonAction))
//
//        toolBar.setItems([doneButton,cancelButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
        
        let toolBar = PBPickerViewToolbar(frame        : .zero,
                                          doneAction   : #selector(self.saveDateToTextField),
                                          cancelAction : #selector(self.cancelButtonAction),
                                          presenter    : self)
        
        self.datePickerTextField.inputAccessoryView = toolBar
    }

    @objc func saveDateToTextField() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = PBStrings.DateFormat.default
        self.datePickerTextField.text = dateFormatter.string(from: datePickerView.date)
        
        self.datePickerTextField.endEditing(true)
    }
    
}
